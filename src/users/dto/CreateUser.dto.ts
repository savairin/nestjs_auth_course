//the DTO: reponse for a schema of how your request payload

import { MinLength, IsNotEmpty, IsEmail } from "class-validator";

export class CreateUserDto{

    @IsNotEmpty()
    @MinLength(3)
    username: string;

    @IsNotEmpty()
    @MinLength(8)
    password: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;
}