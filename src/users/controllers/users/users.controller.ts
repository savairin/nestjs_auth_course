import { Body, ClassSerializerInterceptor, Controller, Get, HttpException, HttpStatus, Inject, Param, ParseIntPipe, Post, UseFilters, UseGuards, UseInterceptors, UsePipes, ValidationPipe } from '@nestjs/common';
import { AuthenticatedGuard, LocalAuthGuard } from 'src/auth/utils/LocalGuard';
import { CreateUserDto } from 'src/users/dto/CreateUser.dto';
import { UserNotFoundException } from 'src/users/exceptions/UserNotFound.exception';
import { HttpExceptionFilter } from 'src/users/filters/HttpException.filter';
import { UsersService } from 'src/users/services/users/users.service';
import { SerializedUser } from 'src/users/types/Index';

@Controller('users')
export class UsersController {

    constructor(@Inject('USER_SERVICE') private readonly userService : UsersService){} 

    @UseInterceptors(AuthenticatedGuard)
    @Get('')
    getUser(){
        return this.userService.getUser();
    }
    

    @UseInterceptors(ClassSerializerInterceptor)
    @Get('username/:username')
    getUserByUsername(@Param('username') username : string){
        const data = this.userService.getUserByUsername(username);
        if(data){
            return {
                'success' : true,
                'message' : 'Get users successfully',
                'data'  : new SerializedUser(data)
            };
       }else{
            throw new HttpException(
                'user not found',
                HttpStatus.BAD_REQUEST 
            ); 
       }
    }

    /// for UseInterceptor make sure you don't return password
    @UseFilters(HttpExceptionFilter)
    @UseInterceptors(ClassSerializerInterceptor)
    @Get('id/:id')
    getUserById(@Param('id', ParseIntPipe) id : number){
        const user = this.userService.getUserById(id);
       try{
        if(user){
            return new SerializedUser(user)
        }else{
            throw new UserNotFoundException('not found', 404);
        }
       }catch(err){
            throw new UserNotFoundException('not found', 404);
       }
    }

    @Post('create')
    @UsePipes(ValidationPipe)
    createUser(@Body() createUserDto: CreateUserDto){
        return this.userService.createUser(createUserDto)
    }

}
