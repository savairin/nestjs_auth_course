import { Exclude } from "class-transformer";

 export interface User{

    id : number;
    username : string;
    password : string;
 }

 export class SerializedUser{
   username : string;

   @Exclude()
   password : string;
 
   
   // Serailization : process that happens before object are return from reponse,
   // Ex: sensitive data like password
   constructor(partial : Partial<SerializedUser>){
      Object.assign(this, partial);
      
   }
 }