import {HttpException, HttpStatus } from "@nestjs/common";

export class UserNotFoundException extends HttpException{

    constructor(msg?: String, status?: HttpStatus){
        super(msg || 'User Not found', status || HttpStatus.BAD_REQUEST);
    }
}
 