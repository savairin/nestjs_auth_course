import { Inject, Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/services/users/users.service';
import { comparepassword } from 'src/utils/bcrypt';

@Injectable()
export class AuthService {

    constructor(@Inject('USER_SERVICE') private readonly userService: UsersService){}

    /// Method
    async validateUser(username: string, password: string) {
        const userDB = await this.userService.findUserByUsername(username);
        const match = comparepassword(password, userDB.password);
        if(userDB){
            if(match){
                console.log('password match');
                return userDB;
            }else{
                console.log('password not match');
                return null;
            }
        }
        console.log('General Error');
        return null;
    }
}
