import { Sessions} from "./Session";
import { User } from "./User";

const entities = [User, Sessions];

export {User, Sessions};
export default entities;