
import { Column, Entity, Index, PrimaryColumn } from "typeorm";

@Entity({name : 'sessions'})
export class Sessions{

    @Index()
    @Column('bigint')
    public expiredAt = Date.now()

    @PrimaryColumn('varchar', {length : 255})
    id = '';

    @Column('text')
    json: '';

}