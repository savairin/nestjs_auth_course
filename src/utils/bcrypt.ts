import * as bcrypt from 'bcrypt';

export function encodePassword(rawPassword : string){

    const SALT = bcrypt.genSaltSync(); // generate salt
    return bcrypt.hashSync(rawPassword, SALT );

}


export function comparepassword(rawPassword : string, hash: string){
    return bcrypt.compareSync(rawPassword, hash);
}