import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as session from 'express-session';
import * as passport from 'passport';
import { TypeormStore } from 'connect-typeorm';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api')
  app.use(
    session({
      /* 
          secret: isused to sign the session ID cookie
          can be: strong or array
       */
      name: 'NESTJS_SESSION_ID',
      secret : 'afeqrkqpcqcrwor940nru4ru2r224r429t4t45', 
      resave : false,
      saveUninitialized : false,
      cookie: {
        maxAge: 600000,
    
      },
      // store: new TypeormStore(
      //   {
      //     cleanupLimit: 2,
      //     limitSubquery: false,
      //     ttl: 86400
      //   }
      // )
    }),
  );
  app.use(passport.initialize());
  app.use(passport.session())
  await app.listen(3000);
}
bootstrap();

function getConnection() {
  throw new Error('Function not implemented.');
}

