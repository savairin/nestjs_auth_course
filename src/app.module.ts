import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { CustomersModule } from './customers/customers.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import entities from './typeorm';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    CustomersModule, 
    UsersModule, 
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3309,
      username: 'root',
      password: '',
      database: 'nest_crash_course',
      entities: entities,
      synchronize: true, // shouldn't be used in production - otherwise you can lose production data.
  }),
  AuthModule,  
  PassportModule.register({
    session: true
  }),
  ],
  controllers: [],
  providers: [],  
})
export class AppModule {}
