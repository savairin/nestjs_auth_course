import { MiddlewareConsumer, Module, NestModule, Next, RequestMethod } from '@nestjs/common';
import { CustomersController } from './controllers/customers/customers.controller';
import { CustomersService } from './services/customers/customers.service';
import { ValidateCustomerMiddleware } from './middlewares/validate-customer';
import { ValidateCustomerAccountMiddleWare } from './middlewares/validate-customer-account';
import { NextFunction } from 'express';

@Module({
  controllers: [CustomersController],
  providers: [CustomersService] 
})
export class CustomersModule implements NestModule{

  configure(consumer: MiddlewareConsumer) {

    /// Call entire 
    // consumer.apply(ValidateCustomerMiddleware)
    // .forRoutes(
    //   CustomersController
    // );

    /// call some endpoint
    consumer.apply(
      ValidateCustomerMiddleware, 
      ValidateCustomerAccountMiddleWare,
      (
        req: Request, 
        res: Response, 
        next: NextFunction) => {
          console.log('Here of add more Middleware');
          next()
        }).forRoutes(
      {
        path: 'customers/search/:id',
        method: RequestMethod.GET
      },
      {
        path: 'customers/:id',
        method: RequestMethod.GET
      }
    );

    /// except some endpoint
    consumer.apply(ValidateCustomerMiddleware)
    .exclude(
      {
        path: 'customers',
        method: RequestMethod.GET
      },
      {
        path: 'customers/create',
        method: RequestMethod.POST
      },
    )
    .forRoutes(CustomersController);
  }
}
 