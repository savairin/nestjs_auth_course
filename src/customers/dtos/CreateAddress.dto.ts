import { IsNotEmpty } from "class-validator";


export class CreateAddressDto{

    @IsNotEmpty() /// required
    line1 : string;

    /// optional - nullable
    line2? : string;

    @IsNotEmpty() /// required
    zip : string;
    @IsNotEmpty() /// required
    city : string;
    @IsNotEmpty() /// required
    state : string; 
}