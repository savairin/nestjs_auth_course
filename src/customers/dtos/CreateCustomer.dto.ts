/// Data Transfer Object

import { IsEmail, IsNotEmpty, IsNotEmptyObject, IsNumber, ValidateNested, isNotEmpty } from "class-validator";
import { CreateAddressDto } from "./CreateAddress.dto";
import { Type } from "class-transformer";

export class CreateCustomerDto {
 
    @IsNumber() /// type : integer
    @IsNotEmpty()  /// required
    id : number;

    @IsNotEmpty() /// required
    name : string;

    @IsEmail() /// type : email
    email : string;

    
    @ValidateNested() /// validate object  
    @Type(() => CreateAddressDto) /// specify type and prov ide type
    @IsNotEmptyObject() /// required object
    address : CreateAddressDto;
 
}  