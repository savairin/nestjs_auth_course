import { Body, Controller, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Req, Res, UsePipes, ValidationPipe } from '@nestjs/common';
import { Request, Response, } from 'express'; 
import { CreateCustomerDto } from 'src/customers/dtos/CreateCustomer.dto';
import { CustomersService } from 'src/customers/services/customers/customers.service';

@Controller('customers')
export class CustomersController {

    constructor(private customersService: CustomersService) {}

    @Get()
    getAllCustomer(@Res() res: Response){
        const data = this.customersService.getAllCustomer();
        return res.send(
            {
                success: true,
                message: 'Get User Successfully',
                data
            }
        );
    }

    @Get(':id')
    getCustomers(
        @Param('id', ParseIntPipe) id : number, 
        @Req() req: Request, 
        @Res() res: Response){
        // console.log(typeof id);
        const data = this.customersService.findCustomerById(id);
        if(data){
            res.send(data);
        }else{
            res.status(400).send({
                success: false,
                messge: 'Customer Not Found',  
            });
        }
    }

    @Get('/search/:id')
    searchCustomerById(
        @Param('id', ParseIntPipe) id : number,
    ){
        const data = this.customersService.findCustomerById(id);
        if(data){
            return {
                success : true,
                message : 'Customer has been found',
                data
            };
        }else{
            
            throw new HttpException('Customer Not found', HttpStatus.BAD_REQUEST)  
        }
    }

    @Post('create')
    @UsePipes(ValidationPipe)
    createCustomer(@Body() createCustomerDto : CreateCustomerDto){
      const data = this.customersService.createCustomer(createCustomerDto)
      
      return {
        success : "true",
        message : "Create User successfully",
      }

    }
        

}
