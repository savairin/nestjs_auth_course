import { Injectable, NestMiddleware } from "@nestjs/common";
import { error } from "console";
import { NextFunction, Request, Response } from "express";

 @Injectable()
 export class ValidateCustomerAccountMiddleWare implements NestMiddleware{

    use(req: Request, res: Response, next: NextFunction ) {

        const{valid_account} =  req.headers;

        console.log('ValidateCustomerAccountMiddleWare')
        if(valid_account){

            next();

        }else{
            return res.status(401 )
            .send( 
                {
                    error : 'Account is invalid'
                }
            );
        }
    }
 }