import { Injectable } from '@nestjs/common';
import { CreateCustomerDto } from 'src/customers/dtos/CreateCustomer.dto';
import { Customer } from 'src/customers/types/Customer';

@Injectable()
export class CustomersService {

    private customers : Customer[] = [
        {
            id : 1,
            name: 'Sovann',
            email : 'savann@example.com',
          
        },
        {
            id : 2,
            name: 'Dalin',
            email : 'dalin@example.com',
        
        },
        {
            id : 3,
            name: 'Ponlok',
            email : 'ponlok@example.com',
        
        },
        {
            id : 4,
            name: 'Boramey',
            email : 'boramey@example.com',
        
        },
        {
            id : 5,
            name: 'Sokea',
            email : 'sokea@example.com',
        
        }
    ]
      
      getAllCustomer(){
        return this.customers;
    }

    findCustomerById(id: number){
        return this.customers.find((customer) => customer.id === id);
    }

    createCustomer(customerDto : CreateCustomerDto ){
        this.customers.push(customerDto);
    }
}
